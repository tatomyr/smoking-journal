#!/bin/bash

# Build & copy code

rm -rf public/

npx typescript --outDir public

cp -R src/index.html src/resources public/

echo "Built"
