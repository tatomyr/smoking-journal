#!/bin/bash

# Specify a correct host
host=https://tatomyr.gitlab.io/purity

if [[ $# -eq 0 ]] ; then
    echo 'Please specify a dependency name.'
    exit 1
fi

echo "Installing $1 from $host"

curl $host/$1.d.ts -o ./src/deps/$1.d.ts

echo "export * from '$host/$1.js'" > ./src/deps/$1.js
