#!/bin/bash

# Compare versions TODO:

old=$(cat public/resources/version.txt)
new=$(cat src/resources/version.txt)
if [ $old == $new ]; then
  echo "Cancelled. The new version should differ from the old one (v$old)."
  exit 64
fi

echo "$old -> $new"

bash bin/build.sh

# Deploy to firebase hosting (smoking-journal.web.app)

firebase deploy --project smoking-journal
