#!/bin/bash

# cd public && npx static-server -p 8090

cd public && deno run --allow-net --allow-read https://deno.land/std/http/file_server.ts --port 8090
