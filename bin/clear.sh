#!/bin/bash

# Find all js files inside src folder
echo "Removing files in src/ folder:"
find ./src -type f -iname '*.js'

# Remove all the js files
find ./src -type f -iname '*.js' -delete
