import {registerRouter} from './deps/router.js'
import {mount, rerender} from './app.js'
import {App} from './components/App.js'

registerRouter(rerender)

mount(App)
