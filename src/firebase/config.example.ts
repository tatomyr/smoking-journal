export const firebaseConfig = {
  apiKey: 'key',
  authDomain: 'smoking-journal.firebaseapp.com',
  databaseURL: 'https://smoking-journal.firebaseio.com',
  projectId: 'smoking-journal',
  storageBucket: 'smoking-journal.appspot.com',
  messagingSenderId: 'id',
  appId: 'id',
  measurementId: 'id',
}
