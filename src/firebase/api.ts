import {getDayEnd, getDayStart} from '../services/dates.js'
import {db} from './firestore.js'
import {Incident} from '../types/app.js'
import {GetResponse, User} from '../types/firebase.js'

export const incidentsAPI = {
  getOnDate: async ({uid}: User, date: number): Promise<Incident[]> => {
    const {docs} = (await db
      .collection('incidents')
      .where('userId', '==', uid)
      .where('date', '>=', getDayStart(date))
      .where('date', '<=', getDayEnd(date))
      .get()) as GetResponse
    return docs.map((item): Incident => ({id: item.id, ...item.data()}))
  },
  add: async ({uid: userId}: User, date: number): Promise<any> =>
    db.collection('incidents').add({userId, date}),
  remove: async (lastIncidentId: string): Promise<any> =>
    db.collection('incidents').doc(lastIncidentId).delete(),
}
