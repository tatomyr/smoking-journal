import {delay} from '../deps/delay.js'
import {push} from '../deps/router.js'
import {setState} from '../app.js'
import {User} from '../types/firebase.js'
import {firebase, firebaseui} from './firebase.js'

// FIXME: refactor, clean up

// export const getCurrentUser = async (): Promise<User> => {
//   const {currentUser} = firebase.auth()
//   if (currentUser) {
//     console.log('USER:', currentUser)
//     return currentUser
//   } else {
//     console.log('AWAITING USER...')
//     await delay(100)
//     return getCurrentUser()
//   }
// }

let u: User | null

export const getCurrentUser = async (): Promise<User | null> => {
  if (u) {
    return u
  } else if (u === null) {
    return null
  } else {
    console.log('AWAITING USER...')
    await delay(100)
    return getCurrentUser()
  }
}

export function registerAuthListener() {
  firebase.auth().onAuthStateChanged(function (user: User | null) {
    if (user) {
      // User is signed in.
      console.log(111111, user)
      setState(() => ({user}))

      u = user
      push('#/journal')
    } else {
      // No user is signed in.
      console.log(2222222, user)
      setState(() => ({user: undefined})) // TODO: do we need this?
      u = null
      push('#/')
    }
  })
}

registerAuthListener()

export const signOut = async () => {
  firebase
    .auth()
    .signOut()
    .then(function () {
      // Delete firebase ui instance
      firebaseui.auth.AuthUI.getInstance().delete()
      // Sign-out successful.
    })
    .catch(function (error: Error) {
      // An error happened.
    })
}
