import {Component} from './purity'
// ----------------------- Hash Router ----------------------

let match: object

export const router = (component: Component) => (props?: object) =>
  component({...match, ...props})

export const getParams = () => match

export const Switch = (routes: {[path: string]: Component}) => {
  for (const path in routes) {
    const params = (path.match(/:(\w+)/g) || []).map((param) => param.slice(1))
    const matchRe = new RegExp(path.replace(/:\w+/g, '(\\w+[\\w\\-\\.]*)'))
    const matches = window.location.hash.match(matchRe)
    // console.log(matches, path, ':', routes[path])
    if (!matches) {
      continue
    }
    const [_, ...args] = matches
    match = params.reduce(($, param, i) => ({...$, [param]: args[i]}), {})
    return routes[path](match)
  }
}

export const registerRouter = (rerender: () => void) => {
  if (window.location.href.indexOf('#/') === -1) {
    window.location.hash = '#/'
  }
  window.onhashchange = rerender
}

export const Redirect = ({to}: {to: string}) => {
  window.location.hash = to
}

export const push = (hash: string) => {
  window.location.hash = hash
}

// TODO: replace search with hash
// export const queryParams = {
//   get: () =>
//     location.search
//       .slice(1)
//       .split('&')
//       .reduce(($, param) => {
//         const [key, value] = param.split('=')
//         return { ...$, [key]: value }
//       }, {}),
//   set: params => {
//     location.search = Object.entries(params)
//       .reduce(($, [key, value]) => `${$}${key}=${value}&`, '?')
//       .slice(0, -1)
//   },
// }

// -------------------- Tab Activity Monitor ------------------

/*
var hidden, state, visibilityChange;
if (typeof document.hidden !== "undefined") {
	hidden = "hidden";
	visibilityChange = "visibilitychange";
	state = "visibilityState";
} else if (typeof document.mozHidden !== "undefined") {
	hidden = "mozHidden";
	visibilityChange = "mozvisibilitychange";
	state = "mozVisibilityState";
} else if (typeof document.msHidden !== "undefined") {
	hidden = "msHidden";
	visibilityChange = "msvisibilitychange";
	state = "msVisibilityState";
} else if (typeof document.webkitHidden !== "undefined") {
	hidden = "webkitHidden";
	visibilityChange = "webkitvisibilitychange";
	state = "webkitVisibilityState";
}

document.addEventListener(visibilityChange, function() {
	console.log(document.hidden)
}, false);
*/
