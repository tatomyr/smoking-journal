export declare type Callback = (...args: any[]) => void;
export declare const debounce: (callback: Callback, wait?: number) => Callback;
