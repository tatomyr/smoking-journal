import {render} from '../deps/purity.js'
import {checkmarkStyle} from '../services/styles.js'
import {getDayProgress} from '../services/dates.js'
import {Incident} from '../types/app.js'

export const Checkmark = ({date}: Incident) => {
  const positionInDay = getDayProgress(date)
  // const positionInDay = (date - getDayStart(date)) / DAY
  // TODO: move to services
  const localTime = new Date(date).toLocaleTimeString().slice(0, -3)

  // TODO: move to services
  const localDay = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'][
    new Date(date).getDay()
  ]

  return render`
    <div
      class="checkmark"
      style="${checkmarkStyle(positionInDay)}"
      title="${localDay} ${localTime}"
      tabindex="0"
    >
      <div class="tooltip ${positionInDay > 0.5 && 'reverse'}">
        ${localDay} ${localTime}
        <button>Change</button>
      </div>
    </div>
  `
}
