import {push} from '../deps/router.js'
import {render} from '../deps/purity.js'
import {getState} from '../app.js'
import {getTodaysIncidents} from '../services/journal.js'
import {getDayProgress} from '../services/dates.js'
import {Today} from './JournalToday.js'
import {JournalActions} from './JournalActions.js'
import {Preloader} from './Preloader.js'

export const JournalPage = () => {
  const {
    status,
    user,
    journal: {incidents},
  } = getState()

  setTimeout(() => {
    if (user && status === 'initial') {
      getTodaysIncidents()
    } else if (user === null) {
      push('#/')
    } else {
      return Preloader()
    }
  })

  const now = Date.now()

  return render`
    <div id="journal">
      ${
        user
          ? render`
            ${Today({
              incidents,
              dayProgress: getDayProgress(now),
            })}
            ${JournalActions()}
          `
          : '<div class="centered">Authenticating...</div>'
      }
      <div style="text-align: center">
        <a href="#/stats">Stats</a>
      </div>
    </div>
  `
}
