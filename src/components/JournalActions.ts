import {render} from '../deps/purity.js'
import {getState} from '../app.js'
import {addIncident, removeIncident} from '../services/journal.js'
import {useLastIncidentTimeout} from '../services/incidents.js'

export const JournalActions = () => {
  const lastIncidentTimeout = useLastIncidentTimeout()
  const {status} = getState()

  console.log('State in Journal Page-->', getState())

  return render`
    <div id="journal-actions" class='actions'>
      <button
        id="add-incident"
        ::click=${addIncident}
        ${
          (!!lastIncidentTimeout ||
            status === 'pending' ||
            status === 'initial') &&
          'disabled'
        }
      >
        ${
          lastIncidentTimeout
            ? `Wait for ${lastIncidentTimeout} seconds`
            : 'Take a cigarette'
        }
      </button>
      ${
        !!lastIncidentTimeout &&
        render`
          <button
            id="remove-incident"
            ::click=${removeIncident}
            ${status === 'pending' && 'disabled'}
          >
            Rewind
          </button>
        `
      }
    </div>
  `
}
