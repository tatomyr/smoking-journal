import {getState, setState} from '../app.js'
import {render} from '../deps/purity.js'

export const Error = () => {
  const {status, error} = getState()
  return (
    error &&
    status === 'failed' &&
    render`
      <div
        id="error"
        class="error-message"
        style="text-align: center; padding: 20px; background-color: red; color: white;"
        ::click=${() => setState(() => ({error: ''}))}
      >
        ${error}
      </div>
    `
  )
}
