import {render} from '../deps/purity.js'
import {getState} from '../app.js'
import {signOut} from '../firebase/auth.js'

export const Header = () => {
  const {user, version} = getState()

  return render`
    <header class="header">
      <h1 id="title" class="title">
        Smoking Journal ${
          version && `<span class="title-small">v${version}</span>`
        }
      </h1>
      <div id="avatar">
        ${
          user &&
          render`
            <img
              class="avatar"
              src="${user.providerData[0].photoURL}"
              loading="lazy"
              alt="${user.providerData[0].displayName}"
              title="${user.providerData[0].displayName}"
              ::click=${(e): void => {
                if (confirm('Do you want to log out?')) {
                  signOut()
                }
              }}
            />
          `
        }
      </div>
    </header>
  `
}
