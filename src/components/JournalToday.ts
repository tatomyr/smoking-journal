import {render} from '../deps/purity.js'
import {Checkmark} from './Checkmark.js'
import {dayStyle} from '../services/styles.js'
import {Incident} from '../types/app.js'
// import {DayType} from '../services/incidents.js'

export type TodayType = {
  // dayStartTime: number
  incidents: Incident[]
  // isToday: boolean
  dayProgress: number
}

export const Today = ({incidents, dayProgress}: TodayType) => render`
  <div ${`id="today"`} class="day">
    ${incidents.map(Checkmark)}
    <div
      ${`id="today-background"`}
      class="day-background"
      style="${dayStyle(incidents.length, dayProgress)}"
    ></div>
  </div>
`
