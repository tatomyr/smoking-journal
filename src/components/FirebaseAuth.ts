import {push} from '../deps/router.js'
import {render} from '../deps/purity.js'
import {getState} from '../app.js'
import {registerFirebaseUI} from '../firebase/register-firebaseui.js'
import {getCurrentUser} from '../firebase/auth.js'

export const LoginPage = () => {
  const {user} = getState()

  setTimeout(() => {
    registerFirebaseUI(getCurrentUser())
  })

  return render`
    <div class="centered">
      <h2 class="title">Please log in using one of the accounts below</h2>
      <div id="firebaseui-auth-container"></div>
    </div>
  `
}
