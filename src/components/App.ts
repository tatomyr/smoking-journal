import {Switch} from '../deps/router.js'
import {render} from '../deps/purity.js'
import {Header} from './Header.js'
import {Error} from './Error.js'
import {JournalPage} from './Journal.js'
import {StatsPage} from './Stats.js'
import {LoginPage} from './FirebaseAuth.js'
import {getCurrentUser} from '../firebase/auth.js'
import {setState} from '../app.js'

const startup = async function () {
  // try {
  const user = await getCurrentUser()
  if (user) {
    // dispatch({type: 'SET_USER', user})
    // dispatch({type: 'GET_INCIDENTS__STARTED'})
  } else {
    // dispatch({ })
    // push('#/')
    // window.location.reload()
  }
  // } catch (err) {
  //   alert('Auth error!')
  // }

  try {
    const version = await fetch('./resources/version.txt').then((res) =>
      res.text()
    )
    console.log(88888, {version})
    setState(() => ({version}))
  } catch {
    console.log(-88888)
  }
}

// setTimeout(() => {
//   startup()
// })

const startup_2 = async function () {
  try {
    const version = await fetch('./resources/version.txt').then((res) =>
      res.text()
    )
    console.log(88888, {version})
    setState(() => ({version}))
  } catch {
    console.log(-88888)
  }
}

setTimeout(() => {
  startup_2()
})

export const App = () => render`
  <div id="root">
    ${Header()}
    ${Switch({
      '#/journal': JournalPage,
      '#/stats': StatsPage,
      '#/': LoginPage,
    })}
    ${Error()}
  </div>
`
