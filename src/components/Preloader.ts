import {render} from '../deps/purity.js'

export const Preloader = () => render`
  <div class="preloader"></div>
`
