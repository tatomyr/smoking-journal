import {push} from '../deps/router.js'
import {render} from '../deps/purity.js'
import {isInViewport} from '../deps/visibility-sensor.js'
import {debounce} from '../deps/debounce.js'

import {getState, setState} from '../app.js'
import {Preloader} from './Preloader.js'
import {IncidentsCount} from '../types/app.js'
import {getPreviousStats} from '../services/stats.js'
import {formatDateForChart} from '../services/dates.js'

const lookForParentScrollableElement = (elem: any): any => {
  const parent = elem.parentNode
  if (parent === document.body) {
    return window
  }
  if (
    parent.scrollHeight > parent.clientHeight ||
    parent.scrollWidth > parent.clientWidth
  ) {
    return parent
  }
  return lookForParentScrollableElement(parent)
}

export const trackVisibility = (elem: any, callback: any) => {
  let parent = lookForParentScrollableElement(elem)
  parent.onscroll = debounce(() => {
    callback(isInViewport(elem))
  }, -250)
  callback(isInViewport(elem))
}

export const DateBar = ({date, count}: IncidentsCount) => render`
  <div class="date-chart__bar" >
    <div
      class="date-chart__bar--bar"
      style="height: ${count * 3}%;"
      title="${count}"
    ></div>
    <div class="date-chart__bar--title">${formatDateForChart(date)}</div>
  </div>
`

export const Stub = () => {
  setTimeout(() => {
    // console.log(
    //   33333,
    //   isInViewport(document.getElementById('visibility-sensor'))
    // )
    trackVisibility(document.getElementById('visibility-sensor'), (e: any) =>
      console.log('--->', e)
    )
  })

  return render`
    <div
      class="date-chart__stub"
    >
      <div id="visibility-sensor" style="width: 30px; background: red;">...</div>
    </div>
  `
}

export const DateChart = () => {
  const {stats} = getState()

  return render`
    <div id="date-chart" class="date-chart">
      ${stats.incidentsCount.map(DateBar)}
      ${Stub()}
    </div>
  `
}

// TODO: useEffect | onMount

export const StatsPage = () => {
  const {status, user, stats} = getState()

  // setTimeout(() => {
  //   if (user /* && status !== 'pending' */) {
  //     // dispatch({type: 'GET_INCIDENTS__STARTED'})
  //   } else if (user === null) {
  //     push('#/')
  //   } else {
  //     return Preloader()
  //   }
  // })

  return render`
    <div id="stats">
      ${
        user
          ? render`
            ${DateChart()}
            <div id="stats-actions" class='actions'>
              <button
                ::click=${getPreviousStats}
                ${status === 'pending' && 'disabled'}
              >
                Get Stats
              </button>
            </div>
          `
          : '<div class="centered">Authenticating...</div>'
      }
      <div style="text-align: center">
        <a href="#/journal">Journal</a>
      </div>
    </div>
  `
}
