import {Incident} from './app.js'

export type User = {
  uid: string
  email?: string
  displayName: string
  photoURL: string
  metadata: {
    creationTime: string
    lastSignInTime: string
  }
  providerData: Array<{
    displayName: string
    photoURL: string
  }>
}

export type GetResponse = {
  docs: Array<{
    id: string
    data: () => Omit<Incident, 'id'>
  }>
}
