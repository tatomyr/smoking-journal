import {User} from './firebase.js'

export type Incident = {
  date: number
  userId: string
  id: string
}

export type Status = 'initial' | 'pending' | 'fulfilled' | 'failed'

export type IncidentsCount = {
  date: number
  count: number
}

export type Journal = {
  incidents: Incident[]
  lastIncidentTimeout?: number
}

export type Stats = {
  incidentsCount: IncidentsCount[]
  dateCursor: number
}

export type AppState = {
  version?: string
  user?: User | null
  status: Status
  error: string
  journal: Journal
  stats: Stats
}
