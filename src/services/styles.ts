import {pipe} from '../deps/pipe.js'

export const checkmarkStyle = (positionInDay: number) => `
  margin-left: calc(${100 * positionInDay}% - 2px);
`

const pinkToBrownGradient = (([R, G, B]) => (x: number) => [
  253 - R * x,
  241 - G * x,
  243 - B * x,
])([175, 177, 179])

const transformTo01 = ((K: number) => (n: number): number =>
  1 - Math.exp(-n * K))(0.1)

export const dayStyle = (incidentsNumber: number, dayProgress: number) => `
  background-color: rgb(${pipe(
    transformTo01,
    pinkToBrownGradient
  )(incidentsNumber).join()});
  width: ${dayProgress * 100}%;
`
