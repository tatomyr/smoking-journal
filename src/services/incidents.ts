import {getState, setState} from '../app.js'
import {Incident} from '../types/app.js'
import {SMOKING_INTERVAL} from './dates.js'

export const getLastIncidentDate = (
  incidents: Incident[]
): number | undefined =>
  incidents.length > 0
    ? Math.max(...incidents.map(({date}) => date))
    : undefined

export const getLastIncidentId = (
  incidents: Incident[]
): string | undefined => {
  const lastDate = getLastIncidentDate(incidents)
  return (
    (lastDate && incidents.find(({date}) => date === lastDate)?.id) || undefined
  )
}

export const getLastIncidentTimeout = (incidents: Incident[]): number => {
  const lastIncidentTime = getLastIncidentDate(incidents)
  const lastIncidentTimeout = lastIncidentTime
    ? Math.max(
        Math.floor((SMOKING_INTERVAL - Date.now() + lastIncidentTime) / 1000),
        0
      )
    : 0
  return lastIncidentTimeout
}

export const useLastIncidentTimeout = (): number => {
  const lastIncidentTimeout = getLastIncidentTimeout(
    getState().journal.incidents
  )
  if (lastIncidentTimeout) {
    setTimeout(() => {
      setState(({journal}) => ({journal: {...journal, lastIncidentTimeout}}))
    }, 1000)
  }
  return lastIncidentTimeout
}
