export const MINUTE = 60 * 1000

export const HOUR = 60 * MINUTE

export const DAY = 24 * HOUR

export const SMOKING_INTERVAL = 10 * MINUTE

export const getDayStart = (date: number): number =>
  new Date(new Date(date).setHours(0, 0, 0, 0)).getTime()

export const getDayEnd = (date: number): number =>
  new Date(new Date(date).setHours(23, 59, 59, 999)).getTime()

export const getDayProgress = (date: number): number => {
  const today = getDayStart(date)

  return (date - today) / (getDayEnd(date) - today)
}

export const formatDateForChart = (date: number): string => {
  const stringDate = new Date(date).toString()
  const [day, month, monthDate] = stringDate.split(' ')
  return `${day}\n${month}\n${monthDate}`
}
