import {setState} from '../app.js'

export const handleFailure = (err: Error) => {
  setState(() => ({status: 'failed', error: err.message}))
  console.error(err)
}
