import {getState, setState, updateStats} from '../app.js'
import {incidentsAPI} from '../firebase/api.js'
import {DAY, getDayStart} from './dates.js'
import {handleFailure} from './failure.js'

export const getPreviousStats = async function () {
  try {
    setState(({stats}) => ({
      status: 'pending',
      stats: {...stats, dateCursor: stats.dateCursor - 1 * DAY},
    }))
    const {user, stats} = getState()
    if (user) {
      const count = (await incidentsAPI.getOnDate(user, stats.dateCursor))
        .length
      setState(({stats}) => ({
        status: 'fulfilled',
        stats: {
          ...stats,
          incidentsCount: [
            ...stats.incidentsCount,
            {date: getDayStart(stats.dateCursor), count},
          ],
        },
      }))
      // setState(
      //   updateStats(({incidentsCount}) => ({
      //     incidentsCount: [
      //       ...incidentsCount,
      //       {date: getDayStart(stats.dateCursor), count},
      //     ],
      //   }))
      // )
    } else {
      throw new Error('User is not authenticated')
    }
  } catch (err) {
    handleFailure(err)
  }
}
