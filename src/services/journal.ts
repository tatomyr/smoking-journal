import {getState, setState} from '../app.js'
import {incidentsAPI} from '../firebase/api.js'
import {getLastIncidentId} from './incidents.js'
import {handleFailure} from './failure.js'
import {AppState} from '../types/app.js'

export const getTodaysIncidents = async function () {
  try {
    setState(() => ({status: 'pending'}))
    const {user} = getState()
    if (user) {
      const incidents = await incidentsAPI.getOnDate(user, Date.now())
      setState(
        ({journal}): Partial<AppState> => ({
          status: 'fulfilled',
          journal: {...journal, incidents},
        })
      ) // FIXME: why it doesn't infer the callback type??
    } else {
      throw new Error('User is not authenticated')
    }
  } catch (err) {
    handleFailure(err)
  }
  console.log('State after Get-->', getState())
}

export const addIncident = async function () {
  try {
    setState(() => ({status: 'pending'}))
    const {user} = getState()
    if (user) {
      await incidentsAPI.add(user, Date.now())
      await getTodaysIncidents()
    } else {
      throw new Error('User is not authenticated')
    }
  } catch (err) {
    handleFailure(err)
  }
}

export const removeIncident = async function () {
  try {
    setState(() => ({status: 'pending'}))
    const lastIncidentId = getLastIncidentId(getState().journal.incidents)
    if (lastIncidentId) {
      await incidentsAPI.remove(lastIncidentId)
      setState(({journal}) => ({journal: {...journal, lastIncidentTimeout: 0}}))
      await getTodaysIncidents()
    } else {
      throw new Error('There is no incident to delete')
    }
  } catch (err) {
    handleFailure(err)
  }
  return 123
}
