import {init} from './deps/purity.js'
import {AppState, Stats} from './types/app.js'

export const {mount, getState, setState, rerender} = init<AppState>({
  version: undefined,
  user: undefined,
  status: 'initial',
  error: '',
  journal: {
    incidents: [],
    lastIncidentTimeout: 0,
  },
  stats: {
    incidentsCount: [],
    dateCursor: Date.now(),
  },
})

export const updateStats = (updates: (stats_: Stats) => Partial<Stats>) => ({
  stats,
}: AppState) => ({
  stats: {...stats, ...updates(stats)},
})

// (update: Partial<Stats>) =>
//   setState(({stats}: AppState) => ({
//     stats: {
//       ...stats,
//       ...update,
//     },
//   }))
