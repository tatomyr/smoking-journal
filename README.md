# Smoking Journalling App

Written using [purity](https://github.com/tatomyr/purity).

# Development

Reexport external dependencies from **.js** file inside **deps** folder.

Do not forget to update type declaration file in **deps** folder:
`curl http://<host>/core.d.ts -o ./src/deps/purity.d.ts`
or use the installing script: `bash bin/add.sh <dependency>` (make sure you've set a correct host in the script).

Run compiler: `bash bin/compile.sh`. Run server on port `:8090`: `bash bin/serve.sh`.

# Deployment

Run `bash bin/deploy.sh` to deploy on `https://smoking-journal.web.app/`.
Please note, you need _firebase_ toolkit to be installed on your machine.
Please use `curl -sL https://firebase.tools | bash` to install it.

# Firebase

Make sure to apply firestore security rules:

```
rules_version = '2';
service cloud.firestore {
  match /databases/{database}/documents {
  	match /incidents/{incident} {
    	allow create: if request.auth != null;
      allow read, update, delete: if request.auth != null && request.auth.uid == resource.data.userId;
    }
  }
}
```
